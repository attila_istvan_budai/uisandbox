import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-wizard',
  templateUrl: './wizard.component.html',
  styleUrls: ['./wizard.component.css']
})
export class WizardComponent implements OnInit {

  steps = [{title: 'First', id:'active'},
    {title: 'Second', id:''},
    {title: 'Third', id:''},
    {title: 'Fourth', id:''},
    {title: 'Fifth', id:''}];

    index = 0;

  constructor(){
      
   }

  ngOnInit() {

  }

  next(){
    this.steps[this.index].id = '';
    if(this.index < this.steps.length-1){
      this.index++;
    }
    this.steps[this.index].id = 'active';

  }

  prev(){
    this.steps[this.index].id = '';
      if(this.index > 0){
      this.index--;
    }
     this.steps[this.index].id = 'active';
  }


}
