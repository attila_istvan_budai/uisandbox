import { Injectable } from '@angular/core';
import { Observable} from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import {Http, Response, RequestOptions, Headers} from '@angular/http';


@Injectable()
export class CsvService {
    dataChange: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);

    constructor(private http: Http){

    }

    public loadCsvFromPath(csvPath, callback){
        let myHeaders = new Headers();
        myHeaders.append('Content-Type', 'text/csv; charset=UTF-8');
        let options = new RequestOptions({headers: myHeaders});
        this.http.get(csvPath, options).subscribe(
            success => {
                callback(success);
            },
            err => {
                console.log(err);
            }
        );
    }

    public readCsv(files: FileList, callback){
        var result: string[][] = [];
        if(files && files.length > 0){
            let file: File = files.item(0);
            let reader: FileReader = new FileReader();
            reader.readAsText(file);
            reader.onload = (e) => {
                let csv: string = reader.result;
                result = this.extractData(csv);
                callback(result);
                console.log(result);
            }
            reader.onloadend = (e) => {
                console.log("finished")
            }

        }
    }

    public extractData(data): any {
        let csvData = data;
        let allTextLines = csvData.split(/\r\n|\n/);
        return this.multiDimensionalArrayFromRows(allTextLines)
    }

    public multiDimensionalArrayFromRows(allTextLines): any {
        let headers = allTextLines[0].split(',');
        let lines = [];

        for (let i = 0; i<allTextLines.length; i++){
            let data = allTextLines[i].split(',');
            if(data.length == headers.length){
                let tarr = [];
                for(let j = 0; j< headers.length; j++){
                    tarr.push(data[j]);
                }
                lines.push(tarr);
            }
        }
        return lines;
    }
}