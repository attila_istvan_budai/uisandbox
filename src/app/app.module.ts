import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router'

import { AppComponent } from './app.component';
import { WizardComponent } from './wizard/wizard.component';
import { HomeComponent } from './home/home.component';

import { NvD3Module } from 'ngx-nvd3';
import { ChartComponent } from './chart/chart.component';


const appRoutes: Routes = [
  {path:'', component: HomeComponent},
  {path:'wizard', component: WizardComponent},
   {path:'chart', component: ChartComponent},
  {path:'**', component: HomeComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    WizardComponent,
    HomeComponent,
    ChartComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    NvD3Module
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
