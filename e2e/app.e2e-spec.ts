import { UISandboxPage } from './app.po';

describe('uisandbox App', () => {
  let page: UISandboxPage;

  beforeEach(() => {
    page = new UISandboxPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
